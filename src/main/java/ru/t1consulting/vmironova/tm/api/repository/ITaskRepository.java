package ru.t1consulting.vmironova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId,
                                  @NotNull String projectId);

    @NotNull
    Task create(@NotNull String userId,
                @NotNull String name,
                @NotNull String description);

    @NotNull
    Task create(@NotNull String userId,
                @NotNull String name);

}
