package ru.t1consulting.vmironova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId,
                   @Nullable String name);

    @NotNull
    Project create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description);

    void changeProjectStatusById(@Nullable String userId,
                                 @Nullable String id,
                                 @Nullable Status status);

    void changeProjectStatusByIndex(@Nullable String userId,
                                    @Nullable Integer index,
                                    @Nullable Status status);

    void updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description);

    void updateByIndex(@Nullable String userId,
                       @Nullable Integer index,
                       @Nullable String name,
                       @Nullable String description);

}
