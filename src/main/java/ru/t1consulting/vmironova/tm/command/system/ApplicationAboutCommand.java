package ru.t1consulting.vmironova.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Veronika Mironova");
        System.out.println("E-mail: vmironova@t1-consulting.ru");
        System.out.println("E-mail: mironovavg2@vtb.ru");
    }

}
