package ru.t1consulting.vmironova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.api.service.IAuthService;
import ru.t1consulting.vmironova.tm.api.service.IUserService;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.vmironova.tm.exception.field.LoginEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.PasswordEmptyException;
import ru.t1consulting.vmironova.tm.exception.user.AccessDeniedException;
import ru.t1consulting.vmironova.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1consulting.vmironova.tm.exception.user.PermissionException;
import ru.t1consulting.vmironova.tm.exception.user.UserLockedException;
import ru.t1consulting.vmironova.tm.model.User;
import ru.t1consulting.vmironova.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public User registry(@Nullable final String login,
                         @Nullable final String password,
                         @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@Nullable final String login,
                      @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.getLocked()) throw new UserLockedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new IncorrectLoginOrPasswordException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        assert userId != null;
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
