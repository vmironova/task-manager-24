package ru.t1consulting.vmironova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.repository.ITaskRepository;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId,
                                         @NotNull final String projectId
    ) {
        return models.stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId,
                       @NotNull final String name,
                       @NotNull final String description
    ) {
        @NotNull final Task task = new Task(name, description);
        task.setUserId(userId);
        return Objects.requireNonNull(add(task));
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId,
                       @NotNull final String name
    ) {
        @NotNull final Task task = new Task(name);
        task.setUserId(userId);
        return Objects.requireNonNull(add(task));
    }

}
